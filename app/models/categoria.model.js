module.exports = (sequelize, Sequelize) => {

    const categoria = sequelize.define("Categoria", {
        id:{
            type: Sequelize.BIGINT,
            primaryKey: true,
            autoIncrement: true
        },
        nombre:{
            type: Sequelize.STRING
        }
    })

    return categoria;
}