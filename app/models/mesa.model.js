


const db = require("../models");



module.exports = (sequelize, Sequelize) => {

    const mesa = sequelize.define("Mesa", {
        id:{
            type: Sequelize.BIGINT,
            primaryKey: true,
            autoIncrement: true
        },
        nombre_mesa:{
            type: Sequelize.STRING,
        },
        id_restaurante:{
            type: Sequelize.BIGINT,
            references:{
                model: 'Restaurantes',
                key:'id'
            }
        },
        posicion_x:{
            type: Sequelize.INTEGER
        },
        posicion_y:{
            type: Sequelize.INTEGER
        },
        planta:{
            type: Sequelize.INTEGER
        },
        capacidad:{
            type: Sequelize.INTEGER
        }
    });

    return mesa ;
}