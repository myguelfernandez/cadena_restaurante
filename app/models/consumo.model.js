


const db = require("../models");



module.exports = (sequelize, Sequelize) => {

    const consumo = sequelize.define("Consumo", {
        id:{
            type: Sequelize.BIGINT,
            primaryKey: true,
            autoIncrement: true
        },
        id_mesa:{
            type: Sequelize.BIGINT,
            references:{
                model: 'Mesas',
                key:'id'
            }
        },
        id_cliente:{
            type: Sequelize.BIGINT,
            references:{
                model: 'Clientes',
                key:'id'
            }
        },
        estado:{
            type: Sequelize.STRING
        },
        total:{
            type:Sequelize.INTEGER
        },
        fecha_creacion:{
            type: Sequelize.DATE,
            defaultValue: new Date()
        },
        fecha_cierre:{
            type: Sequelize.DATE
        }
    });

    return consumo ;
}