
module.exports = (sequelize, Sequelize) => {

    const reserva = sequelize.define("Reserva", {
        id:{
            type: Sequelize.BIGINT,
            primaryKey: true,
            autoIncrement: true
        },
        id_restaurante:{
            type: Sequelize.BIGINT,
            references:{
                model: 'Restaurantes',
                key:'id'
            }
        },
        id_mesa:{
            type: Sequelize.BIGINT,
            references:{
                model: 'Mesas',
                key:'id'
            }
        },
        fecha:{
            type: Sequelize.DATEONLY,
        },
        hora_inicio:{
            type: Sequelize.INTEGER,
        },
        hora_fin:{
            type: Sequelize.INTEGER,
        },
        id_cliente:{
            type: Sequelize.BIGINT,
            references:{
                model: 'Clientes',
                key:'id'
            }
        },
        cantidad_solicitada:{
            type: Sequelize.INTEGER,
        },
    });

    return reserva ;
}