


const db = require("../models");



module.exports = (sequelize, Sequelize) => {

    const detalle_consumo = sequelize.define("DetalleConsumo", {
        id:{
            type: Sequelize.BIGINT,
            primaryKey: true,
            autoIncrement: true
        },
        id_consumo:{
            type: Sequelize.BIGINT,
            references:{
                model: 'Consumos',
                key:'id'
            }
        },
        id_producto:{
            type: Sequelize.BIGINT,
            references:{
                model: 'Productos',
                key:'id'
            }
        },
        cantidad:{
            type:Sequelize.INTEGER
        }
    });

    return detalle_consumo ;
}