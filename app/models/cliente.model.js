module.exports = (sequelize, Sequelize) => {

    const cliente = sequelize.define("Cliente", {
        id:{
            type: Sequelize.BIGINT,
            primaryKey: true,
            autoIncrement: true
        },
        cedula:{
            type: Sequelize.STRING,
            unique: true 
        },
        nombre:{
            type: Sequelize.STRING
        },
        apellido:{
            type: Sequelize.STRING
        }
    })

    return cliente;
}