const db = require("../models");

const Producto = db.Producto;

const Op = db.Sequelize.Op;

//GET
exports.findAll = (req, res) => {

    console.log("entro aqui")

    const nombre_producto = req.query.nombre_producto;
    


    console.log(nombre_producto);

    

    

    var condicion = {}

    if(nombre_producto){
        condicion.nombre = nombre_producto ? { [Op.iLike]: `%${nombre_producto}%` }:null;
    }
    
    console.log(condicion);
    
    

    Producto.findAll({ where: condicion })

        .then(data => {

            res.send(data);

        })

        .catch(err => {

            res.status(500).send({

                message:

                    err.message || "Ocurrio un error al obtener los categorias."

            });

        });

};

//POST
exports.create = (req,res) => {


    const producto = {
        nombre_producto: req.body.nombre_producto,
        precio_venta: req.body.precio_venta,
        id_categoria: req.body.id_categoria
    }

console.log(producto);

    Producto.create(producto).then(
        (data) => {
            console.log(data);
            res.send(data);
        }
    ).catch(
        (err) => {
            res.status(500).send({

                message:

                    err.message || "Ha ocurrido un error al crear un producto."

            });
        } 
    );


}


//PUT
exports.update = (req,res) => {


    const producto = {
        nombre_producto:req.body.nombre_producto,
        precio_venta: req.body.precio_venta,
        id_categoria:req.body.id_categoria
    }

    const condicion = {
        where:{
            id:req.body.id
        }
    }


    Producto.update(producto,condicion)
    .then(
        (data) => {
            console.log(data);
            res.send(data);
        }
    ).catch(
        (err) => {
            res.status(500).send({

                message:

                    err.message || "Ha ocurrido un error al actualizar una categoria."

            });
        } 
    );
}

//DELETE
exports.destroy = (req,res) => {


        
        const condicion = {
            where:{
                id:req.body.id
            }
        }
    
    
        Producto.destroy(condicion)
        .then(
            (data) => {
                console.log("===========================================================")
                console.log(data);
                res.send({
                    registros_eliminados: data
                });
            }
        ).catch(
            (err) => {
                res.status(500).send({
    
                    message:
    
                        err.message || "Ha ocurrido un error al borrar una mesa."
    
                });
            } 
        );
    

}



