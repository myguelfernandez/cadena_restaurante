const db = require("../models");

const Mesa = db.Mesa;
const Restaurante = db.Restaurante;

const Op = db.Sequelize.Op;

//GET
exports.findAll = (req, res) => {

    console.log("entro aqui")

    const nombre = req.query.nombre;
    const id_restaurante = req.query.id_restaurante;

    const cantidad = req.query.cantidad;

    var condition = {};
    
    if(nombre){
        condition.nombre = { [Op.iLike]: `%${nombre}%` } ;
    }
    //var condition = nombre ? { nombre: { [Op.iLike]: `%${nombre}%` } } : null;
    if(id_restaurante){
        condition.id_restaurante = id_restaurante
    }
    if(cantidad){
        condition.capacidad = {[Op.gte]: cantidad}
    }

    Mesa.findAll({ where: condition })

        .then(data => {

            res.send(data);

        })

        .catch(err => {

            res.status(500).send({

                message:

                    err.message || "Ocurrio un error  obtener las mesas."

            });

        });

};

//POST
exports.create = (req,res) => {

    var existe = false;
    console.log("====>" + req.body.id_restaurante);

    

        const mesa = {
            nombre_mesa:req.body.nombre_mesa,
            id_restaurante:req.body.id_restaurante,
            posicion_x:req.body.posicion_x,
            posicion_y:req.body.posicion_y,
            planta:req.body.planta,
            capacidad:req.body.capacidad
        }
    
    
    
        //validaciones varias como verificar horas, cantidad mesas, disponilidad
    
        Mesa.create(mesa).then(
            (data) => {
                console.log(data);
                res.send(data);
            }
        ).catch(
            (err) => {
                res.status(500).send({
    
                    message:
    
                        err.message || "Ha ocurrido un error al crear una mesa."
    
                });
            } 
        );

    

    





    

}

//PUT
exports.update = (req,res) => {


    const mesa = {
        nombre_mesa:req.body.nombre_mesa,
        id_restaurante:req.body.id_restaurante,
        posicion_x:req.body.posicion_x,
        posicion_y:req.body.posicion_y,
        planta:req.body.planta,
        capacidad:req.body.capacidad
    }

    const condicion = {
        where:{
            id:req.body.id
        }
    }


    Mesa.update(mesa,condicion)
    .then(
        (data) => {
            console.log(data);
            res.send(data);
        }
    ).catch(
        (err) => {
            res.status(500).send({

                message:

                    err.message || "Ha ocurrido un error al actualizar una mesa."

            });
        } 
    );
}

//DELETE
exports.destroy = (req,res) => {


        
        const condicion = {
            where:{
                id:req.body.id
            }
        }
    
    
        Mesa.destroy(condicion)
        .then(
            (data) => {
                console.log("===========================================================")
                console.log(data);
                res.send({
                    registros_eliminados: data
                });
            }
        ).catch(
            (err) => {
                res.status(500).send({
    
                    message:
    
                        err.message || "Ha ocurrido un error al borrar una mesa."
    
                });
            } 
        );
    

}

